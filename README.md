# Contacts Directory

Create a contacts directory, displayed in a table, with sortable fields and pagination.

## Main Technology

- React
- Redux - reselect - sagas
- SASS (using BEM)

### JSON structure

The contacts directory information is stored in JSON. Each record includes the following fields:

- id
- first_name
- last_name
- email
- date_of_birth
- industry
- salary
- years_of_experience

## Installation

The JSON file is served via json-server on port 3001. To run the server:

    npm run server

The front end runs on port 3000:

     npm start
