import { createSelector } from "reselect";

const getContactsState = ({ contacts = {} }) => contacts;

export const selectContactsList = createSelector(
  getContactsState,
  ({ contacts = [] }) => contacts
);
export const selectContactsIsLoading = createSelector(
  getContactsState,
  ({ isLoading }) => isLoading === true
);
export const selectContactsMaxPageSize = createSelector(
  getContactsState,
  ({ maxPageSize }) => maxPageSize
);
export const selectContactsSortingOrder = createSelector(
  getContactsState,
  ({ sortOrder }) => sortOrder
);
export const selectContactsSortingType = createSelector(
  getContactsState,
  ({ sortType }) => sortType
);
export const selectContactsPageCount = createSelector(
  getContactsState,
  ({ pageCount }) => pageCount
);
export const selectContactsCurrentPage = createSelector(
  getContactsState,
  ({ currentPage }) => currentPage
);
