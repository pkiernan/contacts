import {
  LOAD_CONTACTS_CALL,
  LOAD_CONTACTS_SUCCESS,
  LOAD_CONTACTS_FAILED
} from "../constants";

export const loadContactsCall = payload => ({
  type: LOAD_CONTACTS_CALL,
  payload
});
export const loadContactsSuccess = payload => ({
  type: LOAD_CONTACTS_SUCCESS,
  payload
});
export const loadContactsFailed = () => ({
  type: LOAD_CONTACTS_FAILED
});
