export const formatAsCurrency = (value = 0) =>
  new Intl.NumberFormat("en-GB", {
    style: "currency",
    currency: "GBP",
    minimumFractionDigits: 2
  }).format(value);

export const formatFullName = ({ first_name = "", last_name = "" }) => {
  const first = first_name && first_name.length ? `${first_name} ` : "";
  const last = last_name && last_name.length ? `${last_name}` : "";
  return `${first}${last}`;
};
