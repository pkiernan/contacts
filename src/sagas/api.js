import { put, delay } from "redux-saga/effects";
import axios from "axios";

function convertGetObjectToQueryString(getData) {
  const keys = Object.keys(getData);
  if (!keys.length) return "";
  const qs = Object.keys(getData)
    .map(key => {
      const rawValue = getData[key];
      const value =
        typeof rawValue === "string" ? rawValue : JSON.stringify(rawValue);
      return `${key}=${encodeURIComponent(value)}`;
    })
    .join("&");
  return `?${qs}`;
}

function getAxiosObject({ uri, method, postData, getData }) {
  const qs = convertGetObjectToQueryString(getData);
  const url = `http://localhost:3001/${uri}${qs}`;
  return {
    method,
    postData,
    getData,
    url
  };
}

export function* callAPI({
  uri = "",
  method = "get",
  postData = {},
  getData = {},
  onSuccess = () => false,
  onFailed = () => false
}) {
  try {
    const axiosConfig = getAxiosObject({
      uri,
      method,
      getData,
      postData
    });
    const response = yield axios(axiosConfig);
    console.log("** response:", response);
    yield delay(1200);
    yield put(onSuccess(response));
  } catch (error) {
    console.log("** error:", error);
    yield put(onFailed(error));
  }
}
