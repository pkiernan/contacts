import { all, takeLatest, fork } from "redux-saga/effects";
import { LOAD_CONTACTS_CALL } from "../constants";
import { getContacts } from "./contacts";

function* watchContacts() {
  yield takeLatest(LOAD_CONTACTS_CALL, getContacts);
}

export default function* root() {
  yield all([fork(watchContacts)]);
}
