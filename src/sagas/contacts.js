import { select } from "redux-saga/effects";
import { callAPI } from "./api";
import { loadContactsFailed, loadContactsSuccess } from "../actions";
import {
  selectContactsMaxPageSize,
  selectContactsSortingOrder,
  selectContactsSortingType
} from "../selectors";
import {
  SORT_TYPE_INCOME,
  SORT_TYPE_INDUSTRY,
  SORT_TYPE_NAME
} from "../constants";

const uri = "contacts";

function* getSortingType() {
  const type = yield select(selectContactsSortingType);
  switch (type) {
    case SORT_TYPE_INCOME:
      return "salary";
    case SORT_TYPE_INDUSTRY:
      return "industry";
    case SORT_TYPE_NAME:
    default:
      return "first_name, last_name";
  }
}

export function* getContacts() {
  yield callAPI({
    uri,
    method: "get",
    onSuccess: loadContactsSuccess,
    onFailed: loadContactsFailed,
    getData: {
      _limit: yield select(selectContactsMaxPageSize),
      _sort: yield getSortingType(),
      _order: yield select(selectContactsSortingOrder)
    }
  });
}
