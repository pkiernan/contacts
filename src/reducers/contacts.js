import { get } from "lodash";
import {
  LOAD_CONTACTS_CALL,
  LOAD_CONTACTS_FAILED,
  LOAD_CONTACTS_SUCCESS,
  SORT_TYPE_NAME,
  SORT_ORDER_ASC
} from "../constants";

const initState = {
  maxPageSize: 20,
  isLoading: false,
  contacts: [],
  sortType: SORT_TYPE_NAME,
  sortOrder: SORT_ORDER_ASC,
  pageCount: 0,
  currentPage: 1
};

export default (state = initState, action) => {
  const { type, payload = {} } = action;
  switch (type) {
    case LOAD_CONTACTS_CALL:
      const {
        sortOrder = initState.sortOrder,
        sortType = initState.sortType,
        currentPage = initState.currentPage
      } = payload;
      return {
        ...state,
        isLoading: true,
        contacts: [],
        sortOrder,
        sortType,
        currentPage
      };
    case LOAD_CONTACTS_SUCCESS:
      const maxResults =
        get(payload, "headers.x-total-count") || state.maxPageSize;
      const pageCount = Math.ceil(Number(maxResults) / state.maxPageSize);
      return {
        ...state,
        isLoading: false,
        contacts: payload.data,
        pageCount
      };
    case LOAD_CONTACTS_FAILED:
      return {
        ...state,
        isLoading: false,
        contacts: []
      };
    default:
      return state;
  }
};
