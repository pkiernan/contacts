import React from "react";
import moment from "moment";
import ContactsTable from "./components/ContactsTable";
import "./App.scss";

function App() {
  return (
    <div className="App">
      <header className="app-header">
        <h1>Contacts Directory</h1>
        <span>{moment().format("MMMM Do YYYY")}</span>
      </header>
      <ContactsTable />
    </div>
  );
}

export default App;
