import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { AiFillCaretLeft, AiFillCaretRight } from "react-icons/ai";
const MAXIMUM_PAGES = 6;

class Pagination extends PureComponent {
  static propTypes = {
    changePage: PropTypes.func.isRequired,
    currentPage: PropTypes.number,
    totalPages: PropTypes.number
  };
  static defaultProps = {
    currentPage: 1,
    totalPages: 1
  };
  constructor(props) {
    super(props);
    const pages = this.getVisiblePageNumbers(props);
    this.state = {
      pages
    };
  }

  changePage(pageNumber) {
    const { changePage } = this.props;
    changePage(pageNumber);
    window.location.hash = pageNumber;
  }

  getVisiblePageNumbers(props) {
    const { currentPage, totalPages } = props;
    const pages = [1];
    if (currentPage < MAXIMUM_PAGES - 1) {
      const maxPages = totalPages < MAXIMUM_PAGES ? totalPages : MAXIMUM_PAGES;
      for (let i = 2; i < maxPages; i++) {
        pages.push(i);
      }
      if (totalPages > MAXIMUM_PAGES) {
        pages.push(null);
      }
    } else if (currentPage > totalPages - MAXIMUM_PAGES + 2) {
      pages.push(null);
      for (let i = totalPages - MAXIMUM_PAGES + 2; i < totalPages; i++) {
        pages.push(i);
      }
    } else {
      pages.push(
        ...[null, currentPage - 1, currentPage, currentPage + 1, null]
      );
    }
    pages.push(totalPages);
    return pages;
  }

  componentWillReceiveProps(props) {
    this.setState({
      pages: this.getVisiblePageNumbers(props)
    });
  }

  renderPageItem({ pageNumber = 1, index = 0 }) {
    const { currentPage } = this.props;
    const isCurrentPage = pageNumber === currentPage;
    const className = `pagination__item pagination__item--page ${
      isCurrentPage ? "is-current" : ""
    }`;
    return (
      <li key={index} className={className}>
        {pageNumber === null ? (
          "..."
        ) : (
          <button
            className="pagination__link"
            onClick={() => this.changePage(pageNumber)}
          >
            {pageNumber}
          </button>
        )}
      </li>
    );
  }

  renderPagePrevious() {
    const { currentPage } = this.props;
    const isDisabled = currentPage === 1;
    const className = `pagination__item pagination__item--previous ${
      isDisabled ? "is-disabled" : ""
    }`;
    const pageNumber = currentPage === 1 ? 1 : currentPage - 1;
    return (
      <li key={"previous"} className={className}>
        <button
          className={`pagination__link ${isDisabled ? "is-disabled" : ""}`}
          onClick={() => this.changePage(pageNumber)}
        >
          <AiFillCaretLeft />
        </button>
      </li>
    );
  }

  renderPageNext() {
    const { currentPage, totalPages } = this.props;
    const isDisabled = currentPage === totalPages;
    const className = `pagination__item pagination__item--next ${
      isDisabled ? "is-disabled" : ""
    }`;
    const pageNumber =
      currentPage === totalPages ? totalPages : currentPage + 1;
    return (
      <li key={"next"} className={className}>
        <button
          className={`pagination__link ${isDisabled ? "is-disabled" : ""}`}
          onClick={() => this.changePage(pageNumber)}
        >
          <AiFillCaretRight />
        </button>
      </li>
    );
  }

  renderPageItems() {
    const { pages } = this.state;
    return [
      this.renderPagePrevious(),
      ...pages.map((pageNumber, index) =>
        this.renderPageItem({ pageNumber, index })
      ),
      this.renderPageNext()
    ];
  }

  render() {
    const { totalPages } = this.props;
    return totalPages > 1 ? (
      <nav className="pagination">
        <ul className="pagination__list">{this.renderPageItems()}</ul>
      </nav>
    ) : null;
  }
}

export default Pagination;
