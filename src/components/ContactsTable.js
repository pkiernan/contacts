import React, { Component } from "react";
import { connect } from "react-redux";
import { loadContactsCall } from "../actions";
import {
  selectContactsList,
  selectContactsIsLoading,
  selectContactsSortingOrder,
  selectContactsSortingType,
  selectContactsPageCount,
  selectContactsCurrentPage
} from "../selectors";
import {
  SORT_TYPE_NAME,
  SORT_TYPE_INCOME,
  SORT_TYPE_INDUSTRY,
  SORT_ORDER_ASC,
  SORT_ORDER_DESC
} from "../constants";
import {
  AiOutlineLoading3Quarters,
  AiFillCaretDown,
  AiFillCaretUp
} from "react-icons/ai";
import { formatAsCurrency, formatFullName } from "../utils";
import Pagination from "./Pagination";

class ContactsTable extends Component {
  componentDidMount() {
    const { loadContacts } = this.props;
    loadContacts();
  }
  renderLoading() {
    const { isLoading } = this.props;
    return isLoading ? (
      <span className="loading-wheel">
        <AiOutlineLoading3Quarters />
      </span>
    ) : null;
  }
  sortTable(sortType) {
    const { sortingOrder, sortingType, loadContacts } = this.props;
    let sortOrder = SORT_ORDER_ASC;
    if (sortingType === sortType) {
      sortOrder =
        sortingOrder === SORT_ORDER_ASC ? SORT_ORDER_DESC : SORT_ORDER_ASC;
    }
    loadContacts({
      sortType,
      sortOrder
    });
  }
  renderTableHead({ label, sortType = null }) {
    const { sortingOrder, sortingType } = this.props;
    const isSortType = sortingType === sortType;
    let className = `sorting-icons ${isSortType ? sortingOrder : ""}`;
    return sortType !== null ? (
      <th>
        <button onClick={() => this.sortTable(sortType)}>
          {label}
          <span className={className}>
            <AiFillCaretUp className="sort-desc" />
            <AiFillCaretDown className="sort-asc" />
          </span>
        </button>
      </th>
    ) : (
      <th>
        <label>{label}</label>
      </th>
    );
  }
  renderTable() {
    const { contactsList, isLoading } = this.props;
    const rows = contactsList.map(
      (
        { first_name, last_name, date_of_birth, industry, salary, email },
        index
      ) => (
        <tr key={`${email}_${index}`}>
          <td>{formatFullName({ first_name, last_name })}</td>
          <td>{date_of_birth}</td>
          <td>{industry}</td>
          <td className="salary">{formatAsCurrency(salary)}</td>
        </tr>
      )
    );
    return !isLoading ? (
      <table className="contacts-table">
        <thead>
          <tr>
            {this.renderTableHead({ label: "Name", sortType: SORT_TYPE_NAME })}
            {this.renderTableHead({ label: "DOB" })}
            {this.renderTableHead({
              label: "Industry",
              sortType: SORT_TYPE_INDUSTRY
            })}
            {this.renderTableHead({
              label: "Annual income",
              sortType: SORT_TYPE_INCOME
            })}
          </tr>
        </thead>
        <tbody>{rows}</tbody>
      </table>
    ) : null;
  }
  renderPagination() {
    const { pageCount: totalPages, currentPage, loadContacts } = this.props;
    const props = {
      totalPages,
      currentPage,
      changePage: page =>
        page !== currentPage ? loadContacts({ currentPage: page }) : false
    };
    return <Pagination {...props} />;
  }
  render() {
    console.log("** props:", this.props);
    return (
      <section className="contacts-table container">
        {this.renderLoading()}
        {this.renderTable()}
        {this.renderPagination()}
      </section>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  loadContacts: data => dispatch(loadContactsCall(data))
});

const mapStateToProps = state => ({
  isLoading: selectContactsIsLoading(state),
  contactsList: selectContactsList(state),
  sortingOrder: selectContactsSortingOrder(state),
  sortingType: selectContactsSortingType(state),
  pageCount: selectContactsPageCount(state),
  currentPage: selectContactsCurrentPage(state)
});

export default connect(mapStateToProps, mapDispatchToProps)(ContactsTable);
